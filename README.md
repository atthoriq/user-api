# Requirements
## Features
Implement a user manager API for user to:

-  [x] Register User
-  [x] Login
-  [x] Edit nickname
-  [x] Update Profile Picture
-  [x] Get his / her profile
-  [x] For simplycity, user information includes: username (cannot be changed), nickname, profile picture.

## Completion
After you finishing the application feature:

-  [x] Make sure there are at least 5 million user accounts in the test database
-  [x] Install WRK (https://github.com/wg/wrk) and try to loadtest your Get Profile Endpoint. See the RPS and lattency, try to optimize this both metrics
```bash
$ wrk -c400 -t12 -d30s --latency http://127.0.0.1:8080/profile/username%20999

Running 30s test @ http://127.0.0.1:8080/profile/username%20999
  12 threads and 400 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     7.70ms    1.74ms  82.18ms   91.17%
    Req/Sec     4.21k     0.87k   51.43k    97.17%
  Latency Distribution
     50%    7.74ms
     75%    8.14ms
     90%    8.61ms
     99%    9.89ms
  1509377 requests in 30.10s, 328.20MB read
  Socket errors: connect 0, read 249, write 0, timeout 0
Requests/sec:  50139.31
Transfer/sec:     10.90MB
```
-  [ ] Schedule the demo with your manager

## Time Limit
-  [ ] You need to finish this task in 5 working days.

**Overdue.**

## Environment
Your Local PC
DB: MySQL 5.5 or above

## Design Constraint
-  [x] Authentication logic on HTTP Server.

I use Jason Web Token as a token.

-  [x] User information must be stored in a MySQL database. Connect by MySQL Go client.
-  [x] Create your repository according to Standard Go Project Layout

Not sure about the structure.

-  [x] Since you are building Webserver, pls use HttpRouter
-  [x] 70% unit test code coverage verified through GitLab CI.

Already put test in Gitlab CI. By this far, the test only cover three layer of design:
- Deliver layer -> [handler](https://gitlab.com/atthoriq/user-api/tree/master/pkg/handler)
- Usecase layer -> [service](https://gitlab.com/atthoriq/user-api/tree/master/pkg/service)
- Repository layer -> [data](https://gitlab.com/atthoriq/user-api/tree/master/pkg/data)


## Deliverables
-  [x] Project source
-  [x] [Database schema design](https://gitlab.com/atthoriq/user-api/configs/db.configs.go)


# API
### **Register User**
`/regist`

method `POST`

request body:
```bash
{
	"Username": "string",
	"Password": "string",
	"NickName": "string"
}
```
response:
```bash
{
    "code": 200,
    "message": "Success",
    "result": {
        "id": int,
        "username": "string",
        "nickname": "string",
        "profile_pic": null // not initialized yet
    }
}
```
### **Login**
`/login`

method `POST`

request body:
```
{
	"Username": "string",
	"Password": "string"
}
```
response:
```bash
{
    "code": 200,
    "message": "Success",
    "result": {
        "token": "eabcdefgh.....",
        "user": {
            "id": int,
            "username": "string",
            "nickname": "string",
            "profile_pic": null or "string"
        }
    }
}
```
### **Edit nickname**
`/updateName`

method `POST`

request header:`authorization: Bearer token`

*token get when logged in*

request body:
```
{
	"newName" : "string"
}
```
response:
```bash
{
    "code": 200,
    "message": "Success",
    "result": "Update successful to 1 row(s)."
}
```
### **Update Profile Picture**
`/updateProfilepic`

method `POST`

request header:`authorization: Bearer token`

*token get when logged in*

request body:
```bash
form-data body

"file" : the_file.png
```
response:
```bash
{
    "code": 200,
    "message": "Success",
    "result": "Update successful, picture path: http://localhost:8080/assets/id.ext"
}
```
### **Get his / her profile**
`/profile/:username`

method `GET`

response:
```bash
{
    "code": 200,
    "message": "Success",
    "result": {
        "id": int,
        "username": "string",
        "nickname": "string",
        "profile_pic": "url string"
    }
}
```