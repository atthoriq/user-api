#!/bin/sh

go clean -testcache

if [ $# -eq 0 ]
then
    go test ./... -cover
elif [ $1 == "-v" ]
then
    go test ./... -cover -v
fi