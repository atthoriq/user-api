package main

import (
	"log"
	"net/http"

	"github.com/julienschmidt/httprouter"

	c "gitlab.com/atthoriq/user-api/pkg/cache"
	"gitlab.com/atthoriq/user-api/pkg/data"
	d "gitlab.com/atthoriq/user-api/pkg/db"
	"gitlab.com/atthoriq/user-api/pkg/handler"
	"gitlab.com/atthoriq/user-api/pkg/helper/middleware"
	// "gitlab.com/atthoriq/user-api/pkg/seed"
	"gitlab.com/atthoriq/user-api/pkg/service"
)

func main() {
	db := d.GetDatabase()
	redis := d.GetCache()
	cache := c.New(redis)
	defer db.Close()
	defer cache.Close()
	repo := data.New(db, cache)
	useCase := service.New(repo)
	controller := handler.New(useCase)

	// seed.Load(db)

	router := httprouter.New()

	router.POST("/login", controller.Login)
	router.POST("/regist", controller.Register)
	router.POST("/updateName", middleware.Authorize(controller.UpdateNickName))
	router.POST("/updateProfilepic", middleware.Authorize(controller.UpdateProfilepic))
	
	router.GET("/profile/:username", controller.GetProfile)
	router.GET("/assets/:file", controller.GetProfilePic)

	log.Print("Ready to serve on port: 8080")
	log.Fatal(http.ListenAndServe(":8080", router))
}

