package contracts

import (
	"gitlab.com/atthoriq/user-api/pkg/constants"
)

type Repository interface {
	Register(user constants.User) (*constants.User,error)
	GetUserByUsername(username string) (*constants.User,error)
	UpdateNickname(nickname string, userid int, username string) (*int64, error)
	UpdateProfilepic(path string, userid int) (*int64, error)
}