package contracts

import (
	"mime/multipart"
	
	"gitlab.com/atthoriq/user-api/pkg/constants"
)

type Usecase interface {
	Register(registerUser constants.RegisterUserPayload) (*constants.UserOutput, error)
	Login(loginPayload constants.LoginPayload) (*constants.LoginResponse, error)
	UpdateNickname(payload constants.UpdateNicknamePayload, userid int, username string) (*string, error)
	UpdateProfilepic(uploadedFile multipart.File, fileext string, userid int) (*string, error)
	GetProfile(username string) (*constants.UserOutput, error)
}