package configs

import (
	"log"
	"os"
	"github.com/joho/godotenv"
)

var (
	serverName string
	user string
	password string
	dbName string
	userTable string
)

func init() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	serverName = os.Getenv("SERVERNAME")
	user = os.Getenv("DBUSER")
	password = os.Getenv("DBPASS")
	dbName = os.Getenv("DBNAME")
	userTable = `CREATE TABLE IF NOT EXISTS users(
		user_id INT AUTO_INCREMENT PRIMARY KEY,
		username VARCHAR(40) UNIQUE,
		password VARCHAR(255),
		nickname VARCHAR(255),
		profile_pic VARCHAR(255) UNIQUE
	 );`
}

func Servername() (string) {
	return serverName
}

func DbUser() (string) {
	return user
}

func DbPassword() (string) {
	return password
}

func DbName() (string) {
	return dbName
}

func UserScheme() (string) {
	return userTable
}
