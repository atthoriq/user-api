package configs

import (
	"log"
	"os"
	"strconv"
	"github.com/joho/godotenv"
)

var (
	redisServer string
	redisPass string
	redisDb int
)

func init() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	redisServer = os.Getenv("REDISSERVER")
	redisPass = os.Getenv("REDISPASS")
	redisDb, err = strconv.Atoi(os.Getenv("REDISDB"))
	if err != nil {
		log.Fatal("Error getting redis config")
	}
}

func CacheServer() (string) {
	return redisServer
}

func CachePassword() (string) {
	return redisPass
}

func CacheDb() (int) {
	return redisDb
}
