package constants

type User struct {
	ID       uint64  `json:"id" db:"id"`
	Username string  `json:"username" db:"username"`
	Nickname string  `json:"nickname" db:"nickname"`
	Password string  `json:"password" db:"password"`
	Profile_Pic    *string `json:"profile_pic" db:"profile_pic"`
}

type UserOutput struct {
	ID       uint64  `json:"id" db:"id"`
	Username string  `json:"username" db:"username"`
	Nickname string  `json:"nickname" db:"nickname"`
	Profile_Pic    *string `json:"profile_pic" db:"profile_pic"`
}

const (
	QInsertUser        = "INSERT INTO users(username, nickname, password) VALUES('%s','%s','%s')"
	QGetUserByUsername = "SELECT user_id, username, nickname, password, profile_pic FROM users where username = ?"
	QUpdateNickname    = "UPDATE users SET nickname = '%s' WHERE user_id = %d"
	QUpdateProfilepic  = "UPDATE users SET profile_pic = '%s' WHERE user_id = %d"
)