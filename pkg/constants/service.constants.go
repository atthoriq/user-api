package constants

type RegisterUserPayload struct {
	Username string `json:"username"`
	Password string `json:"password"`
	Nickname string `json:"nickname"`
}

type LoginPayload struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type UpdateNicknamePayload struct {
	NewName string `json:"newName"`
}

type LoginResponse struct {
	Token	string 		 `json:"token"`
	User	UserOutput `json:"user"`
}