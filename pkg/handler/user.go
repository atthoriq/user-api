package handler

import (
	"fmt"
	"net/http"
	"encoding/json"
	"path/filepath"

	"github.com/julienschmidt/httprouter"
	"gitlab.com/atthoriq/user-api/contracts"
	"gitlab.com/atthoriq/user-api/pkg/constants"
	"gitlab.com/atthoriq/user-api/pkg/helper/errorHandler"
	"gitlab.com/atthoriq/user-api/pkg/helper/jwt"
	"gitlab.com/atthoriq/user-api/pkg/helper/response"
)

type UserHandler struct {
	UserService contracts.Usecase
}

func New(useCase contracts.Usecase) *UserHandler {
	return &UserHandler{useCase}
}

func getToken(r *http.Request) (*jwt.TokenPayload, error) {
	token := r.Context().Value("token")
	if token == nil {
		return nil, errorHandler.GenerateNewError(http.StatusUnauthorized, "An Authorization header is required")
	}

	return token.(*jwt.TokenPayload), nil
}

func (u *UserHandler) Register(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	registerUser := constants.RegisterUserPayload{}
	err := json.NewDecoder(r.Body).Decode(&registerUser)
	if err != nil {
		response.ToJson(w, nil, errorHandler.GenerateError(err))
		return
	}

	user, err := u.UserService.Register(registerUser)
	if err != nil {
		response.ToJson(w, nil, errorHandler.GenerateError(err))
		return
	}

	response.ToJson(w, *user, nil)

}

func (u *UserHandler) Login(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	loginPayload := constants.LoginPayload{}
	err := json.NewDecoder(r.Body).Decode(&loginPayload)
	if err != nil {
		response.ToJson(w, nil, errorHandler.GenerateError(err))
		return
	}

	output, err := u.UserService.Login(loginPayload)
	if err != nil {
		response.ToJson(w, nil, errorHandler.GenerateError(err))
		return
	}

	response.ToJson(w,*output,nil)

}

func (u *UserHandler) UpdateNickName(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	tokenData, err := getToken(r)
	if err != nil {
		response.ToJson(w, nil, errorHandler.GenerateError(err))
		return
	}
	
	var payload constants.UpdateNicknamePayload
	
	err = json.NewDecoder(r.Body).Decode(&payload)
	if err != nil {
		response.ToJson(w, nil, errorHandler.GenerateError(err))
		return
	}

	output, err := u.UserService.UpdateNickname(payload, int(tokenData.ID), tokenData.Username)
	if err != nil {
		response.ToJson(w, nil, errorHandler.GenerateError(err))
		return
	}
	response.ToJson(w, *output, nil)

}

func (u *UserHandler) UpdateProfilepic(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	tokenData, err := getToken(r)
	if err != nil {
		response.ToJson(w, nil, errorHandler.GenerateError(err))
		return
	}

	uploadedFile, handler, err := r.FormFile("file")
	if err != nil {
		response.ToJson(w, nil, errorHandler.GenerateError(err))
		return
	}
	fileExt := filepath.Ext(handler.Filename)
	defer uploadedFile.Close()

	output, err := u.UserService.UpdateProfilepic(uploadedFile, fileExt, int(tokenData.ID))
	if err != nil {
		response.ToJson(w, nil, errorHandler.GenerateError(err))
		return
	}
	response.ToJson(w, *output, nil)

}

func (u *UserHandler) GetProfilePic(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	requestedFile := fmt.Sprintf("assets/%s", ps.ByName("file"))
	
	http.ServeFile(w,r,requestedFile)
}

func (u *UserHandler) GetProfile(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	username := ps.ByName("username")

	output, err := u.UserService.GetProfile(username)
	if err != nil {
		response.ToJson(w, nil, errorHandler.GenerateError(err))
		return
	}

	response.ToJson(w, *output, nil)
}