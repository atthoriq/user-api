package handler_test

import (
	"bytes"
	"fmt"
	"encoding/json"
	"errors"
	"image"
	"image/png"
	"io"
	"mime/multipart"
	"os"
	"path/filepath"
	"testing"
	"net/http"
	"net/http/httptest"

	"github.com/julienschmidt/httprouter"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"

	"gitlab.com/atthoriq/user-api/contracts/mocks"
	"gitlab.com/atthoriq/user-api/pkg/constants"
	h "gitlab.com/atthoriq/user-api/pkg/handler"
	"gitlab.com/atthoriq/user-api/pkg/helper/encrypt"
	"gitlab.com/atthoriq/user-api/pkg/helper/jwt"
	"gitlab.com/atthoriq/user-api/pkg/helper/middleware"
)

const (
	userid = 1
	uname = "uname"
	password = "passwd"
	nickname = "nickname"
	newNick = "nickname2"
)
var userOut = constants.UserOutput{
	ID: userid,
	Username: uname,
	Nickname: nickname,
}

var hashpass, _ = encrypt.EncodePassword(password)
var token, _ = jwt.GenerateToken(jwt.TokenPayload{ID: userid, Username: uname})
var authHeader = fmt.Sprintf("Bearer %s", *token)
var loginResponse = constants.LoginResponse{
	Token: *token,
	User: userOut,
}

func TestRegist(t *testing.T) {
	router := httprouter.New()
	mockUseCase := new(mocks.Usecase)
	handler := h.New(mockUseCase)

	
	router.POST("/regist", handler.Register)
	requestBody, err := json.Marshal(constants.RegisterUserPayload{
		Username: uname,
		Password: password,
		Nickname: nickname,
	})
	if err != nil {
		t.Errorf("error when marshal-ing: %v",err)
	}

	mockUseCase.On("Register", mock.AnythingOfType("RegisterUserPayload")).Return(&userOut, nil)
	
	req, err := http.NewRequest("POST", "/regist", bytes.NewBuffer(requestBody))
	if err != nil {
		t.Errorf("error when requesting: %v",err)
	}

	rr := httptest.NewRecorder()
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusOK, rr.Code)
	mockUseCase.AssertExpectations(t)
}

func TestLogin(t *testing.T) {
	router := httprouter.New()
	mockUseCase := new(mocks.Usecase)
	handler := h.New(mockUseCase)

	
	router.POST("/login", handler.Login)
	requestBody, err := json.Marshal(constants.LoginPayload{
		Username: uname,
		Password: hashpass,
	})
	if err != nil {
		t.Errorf("error when marshal-ing: %v",err)
	}

	mockUseCase.On("Login", mock.AnythingOfType("LoginPayload")).Return(&loginResponse, nil)

	req, err := http.NewRequest("POST", "/login", bytes.NewBuffer(requestBody))
	if err != nil {
		t.Errorf("error when requesting: %v",err)
	}

	rr := httptest.NewRecorder()
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusOK, rr.Code)
	mockUseCase.AssertExpectations(t)
}

func TestInvalidPasswordLogin(t *testing.T) {
	router := httprouter.New()
	mockUseCase := new(mocks.Usecase)
	handler := h.New(mockUseCase)

	
	router.POST("/login", handler.Login)
	requestBody, err := json.Marshal(constants.LoginPayload{
		Username: uname,
		Password: hashpass,
	})
	if err != nil {
		t.Errorf("error when marshal-ing: %v",err)
	}

	mockUseCase.On("Login", mock.AnythingOfType("LoginPayload")).Return(nil, errors.New("some error"))

	req, err := http.NewRequest("POST", "/login", bytes.NewBuffer(requestBody))
	if err != nil {
		t.Errorf("error when requesting: %v",err)
	}

	rr := httptest.NewRecorder()
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusInternalServerError, rr.Code)
	mockUseCase.AssertExpectations(t)
}

func TestUpdateNickname(t *testing.T) {
	router := httprouter.New()
	mockUseCase := new(mocks.Usecase)
	handler := h.New(mockUseCase)
	
	router.POST("/updateName", middleware.Authorize(handler.UpdateNickName))
	requestBody, err := json.Marshal(constants.UpdateNicknamePayload{
		NewName: newNick,
	})
	if err != nil {
		t.Errorf("error when marshal-ing: %v",err)
	}

	expectedOutput := "some string"
	mockUseCase.On("UpdateNickname", mock.AnythingOfType("UpdateNicknamePayload"), mock.AnythingOfType("int"), mock.AnythingOfType("string")).Return(&expectedOutput, nil)

	req, err := http.NewRequest("POST", "/updateName", bytes.NewBuffer(requestBody))
	if err != nil {
		t.Errorf("error when requesting: %v",err)
	}
	req.Header.Set("Authorization", authHeader)
	
	rr := httptest.NewRecorder()
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusOK, rr.Code)
	mockUseCase.AssertExpectations(t)
}

func getFile(path string) (*os.File) {
	// Create a blank image 100x200 pixels
    myImage := image.NewRGBA(image.Rect(0, 0, 100, 200))

    // outputFile is a File type which satisfies Writer interface
    outputFile, err := os.Create(path)
    if err != nil {
    	panic(err)
    }

    // Encode takes a writer interface and an image interface
    // We pass it the File and the RGBA
	png.Encode(outputFile, myImage)
	
	return outputFile
}

func initTestUpdateProfilePict() (*bytes.Buffer, string) {
	ext := ".png"
	filename := fmt.Sprintf("_%s", ext)
	fp := fmt.Sprintf("./%s", filename)

	file := getFile(fp)

    body := &bytes.Buffer{}
    writer := multipart.NewWriter(body)
    part, err := writer.CreateFormFile("file", filepath.Base(file.Name()))
    if err != nil {
        panic(err)
    }

	io.Copy(part, file)
	
	ca := writer.FormDataContentType()
	writer.Close()
	
	return body, ca
}

func closeTestUpdateProfilePic() {
	os.Remove("./_.png")
}

func TestSuccessUpdateProfilePic(t *testing.T) {
	router := httprouter.New()
	mockUseCase := new(mocks.Usecase)
	handler := h.New(mockUseCase)
	
	router.POST("/updateProfilepic", middleware.Authorize(handler.UpdateProfilepic))
	requestBody, ca := initTestUpdateProfilePict()
	defer closeTestUpdateProfilePic()

	expectedOutput := "some string"
	mockUseCase.On("UpdateProfilepic", mock.Anything, mock.AnythingOfType("string"), mock.AnythingOfType("int")).Return(&expectedOutput, nil)

	req, err := http.NewRequest("POST", "/updateProfilepic", requestBody)
	if err != nil {
		t.Errorf("error when requesting: %v",err)
	}
	req.Header.Set("Authorization", authHeader)
	req.Header.Add("Content-Type", ca)
	
	rr := httptest.NewRecorder()
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusOK, rr.Code)
	mockUseCase.AssertExpectations(t)
}

func TestUpdateProfilePicWithNoFile(t *testing.T) {
	router := httprouter.New()
	mockUseCase := new(mocks.Usecase)
	handler := h.New(mockUseCase)
	
	router.POST("/updateProfilepic", middleware.Authorize(handler.UpdateProfilepic))

	req, err := http.NewRequest("POST", "/updateProfilepic", bytes.NewBufferString("{\"some\": \"string\"}"))
	if err != nil {
		t.Errorf("error when requesting: %v",err)
	}
	req.Header.Set("Authorization", authHeader)
	
	rr := httptest.NewRecorder()
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusInternalServerError, rr.Code)
}

func TestGetProfilePicNoPic(t *testing.T) {
	router := httprouter.New()
	mockUseCase := new(mocks.Usecase)
	handler := h.New(mockUseCase)
	
	router.GET("/assets/:file", handler.GetProfilePic)

	req, err := http.NewRequest("GET", "/assets/_", nil)
	if err != nil {
		t.Errorf("error when requesting: %v",err)
	}
	
	rr := httptest.NewRecorder()
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusNotFound, rr.Code)
}

func TestSuccessGetProfile(t *testing.T) {
	router := httprouter.New()
	mockUseCase := new(mocks.Usecase)
	handler := h.New(mockUseCase)
	
	router.GET("/profile/:username", handler.GetProfile)

	mockUseCase.On("GetProfile", mock.AnythingOfType("string")).Return(&userOut, nil)

	req, err := http.NewRequest("GET", "/profile/uname", nil)
	if err != nil {
		t.Errorf("error when requesting: %v",err)
	}
	
	rr := httptest.NewRecorder()
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusOK, rr.Code)
	mockUseCase.AssertExpectations(t)
}

func TestFailGetProfile(t *testing.T) {
	router := httprouter.New()
	mockUseCase := new(mocks.Usecase)
	handler := h.New(mockUseCase)

	router.GET("/profile/:username", handler.GetProfile)

	mockUseCase.On("GetProfile", mock.AnythingOfType("string")).Return(nil, errors.New("some error"))

	req, err := http.NewRequest("GET", "/profile/uname", nil)
	if err != nil {
		t.Errorf("error when requesting: %v",err)
	}
	
	rr := httptest.NewRecorder()
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusInternalServerError, rr.Code)
	mockUseCase.AssertExpectations(t)
}
