package errorHandler

import (
	"fmt"
	"runtime"
)

type ErrorString struct {
	code       int
	stacktrace string
	message    string
}

func (e ErrorString) Code() int {
	return e.code
}

func (e ErrorString) Error() string {
	return e.message
}

func (e ErrorString) Stacktrace() string {
	return e.stacktrace
}

func GenerateError(err error) error {
	_, isErrorString := err.(*ErrorString)

	if isErrorString {
		return err
	}

	stacktrace := fmt.Sprintf("\nMessage: %s", err.Error())

	stack := 1
	pc, file, line, _ := runtime.Caller(stack)
	function := runtime.FuncForPC(pc)

	for line != 0 || function != nil {
		stacktrace += fmt.Sprintf("\n--- at %s:%d ---", file, line)
		
		stack++
		pc, file, line, _ = runtime.Caller(stack)
		function = runtime.FuncForPC(pc)
	}

	errorResponse := ErrorString{
		500, 
		stacktrace, 
		err.Error(),
	}
		
	return &errorResponse
}

func GenerateNewError(code int, message string) error {

	stacktrace := fmt.Sprintf("\nMessage: %s", message)

	stack := 1
	pc, file, line, _ := runtime.Caller(stack)
	function := runtime.FuncForPC(pc)

	for line != 0 || function != nil {
		stacktrace += fmt.Sprintf("\n--- at %s:%d ---", file, line)
		
		stack++
		pc, file, line, _ = runtime.Caller(stack)
		function = runtime.FuncForPC(pc)
	}

	errorResponse := ErrorString{
		code,
		stacktrace,
		message,
	}

	return &errorResponse
}