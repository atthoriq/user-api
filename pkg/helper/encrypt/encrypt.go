package encrypt

import (
	"golang.org/x/crypto/bcrypt"
)

func EncodePassword(pass string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(pass), 14)

	return string(bytes), err
}

func CheckPassword(encodedPass, rawPass string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(encodedPass), []byte(rawPass))

	return err == nil
}