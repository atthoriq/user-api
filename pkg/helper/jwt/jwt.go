package jwt

import (
	"time"
	"net/http"

	"gitlab.com/atthoriq/user-api/pkg/helper/errorHandler"
	jwt "github.com/dgrijalva/jwt-go"
)

type TokenClaim struct {
	ID		 uint64 `json:"id"`
	Username string `json:"username"`
	jwt.StandardClaims
}

type TokenPayload struct {
	ID		 uint64 `json:"id"`
	Username string `json:"username"`
	ExpiresTime uint64 `json:"expires"`
}

func GenerateToken(payload TokenPayload) (*string, error) {
	tokenClaim := TokenClaim{
		payload.ID,
		payload.Username,
		jwt.StandardClaims{
			ExpiresAt: time.Now().AddDate(0, 0, 1).Unix(),
			Issuer: "A System",
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, tokenClaim)
	ss, err := token.SignedString([]byte("thoriqnick"))
	if err != nil {
		return nil, errorHandler.GenerateNewError(500, "Can not generate token")
	}

	return &ss, nil
}

func ParseToken(token string) (*jwt.Token, error) {
	return jwt.Parse(token, func(token *jwt.Token) (interface{}, error) {
        if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
            return nil, errorHandler.GenerateNewError(http.StatusUnauthorized, "Invalid signing method")
        }
        return []byte("thoriqnick"), nil
    })
}

func GetTokenData(token *jwt.Token) (*TokenPayload, error){
	tokenClaims := token.Claims.(jwt.MapClaims)
	if tokenClaims.VerifyExpiresAt(time.Now().Unix(), false) == false {
		return nil, errorHandler.GenerateNewError(http.StatusUnauthorized, "Token is expired")
	}

	id := tokenClaims["id"].(float64)

	tokenData := TokenPayload{
		ID:       uint64(id),
		Username: tokenClaims["username"].(string),
		ExpiresTime: uint64(tokenClaims["exp"].(float64)),
	}

	return &tokenData, nil
}