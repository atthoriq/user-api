package response

import (
	"net/http"
	"encoding/json"

	"gitlab.com/atthoriq/user-api/pkg/helper/errorHandler"
)

type DefaultResponse struct {
	Code    int         `json:"code"`
	Message string     `json:"message"`
	Result    interface{} `json:"result"`
}

const SUCCESS = "Success"

func ToJson(w http.ResponseWriter, output interface{}, err error) {
	w.Header().Set("Content-Type", "application/json")

	if err != nil {
		e, _ := err.(*errorHandler.ErrorString)

		errorMessage := e.Error()
		response := DefaultResponse{
			Code: e.Code(),
			Message: errorMessage,
			Result: e.Stacktrace(),
		}

		bodyResponse, _ := json.Marshal(response)

		w.WriteHeader(response.Code)
		w.Write(bodyResponse)

	} else {

		response := DefaultResponse{
			Code: http.StatusOK,
			Message: SUCCESS,
			Result: output,
		}

		bodyResponse, _ := json.Marshal(response)
	
		w.WriteHeader(http.StatusOK)
		w.Write(bodyResponse)
	}
}