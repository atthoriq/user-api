package middleware

import (
	"context"
	"strings"
	"net/http"

	"gitlab.com/atthoriq/user-api/pkg/helper/jwt"
	"gitlab.com/atthoriq/user-api/pkg/helper/response"
	"gitlab.com/atthoriq/user-api/pkg/helper/errorHandler"
	"github.com/julienschmidt/httprouter"
)


func Authorize(next httprouter.Handle) httprouter.Handle {
	return httprouter.Handle(func(w http.ResponseWriter, req *http.Request, ps httprouter.Params){
		authorizationHeader := req.Header.Get("Authorization")
		if authorizationHeader == "" {
			response.ToJson(w, nil,errorHandler.GenerateNewError(http.StatusUnauthorized, "An Authorization header is required"))
			return
		}
		bearerToken := strings.Split(authorizationHeader, " ")
		if len(bearerToken) != 2 || bearerToken[0] != "Bearer" {
			response.ToJson(w, nil,errorHandler.GenerateNewError(http.StatusBadRequest, "Invalid token"))
			return
		}

		token, err := jwt.ParseToken(bearerToken[1])
		if err != nil {
			response.ToJson(w, nil,errorHandler.GenerateError(err))
			return
		}

		var tokenData *jwt.TokenPayload
		if token != nil && err == nil {
			tokenData, err = jwt.GetTokenData(token)
		}

		ctx := context.WithValue(req.Context(), "token", tokenData)

		next(w, req.WithContext(ctx), ps)
	})
}
