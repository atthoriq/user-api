package fileUploader

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
	"mime/multipart"
	"strconv"

	"gitlab.com/atthoriq/user-api/pkg/helper/errorHandler"
)

const URIBASE = "http://localhost:8080"
const STATICDIR = "assets"

func fileUri(filename string) string {
	return fmt.Sprintf("%s/%s/%s", URIBASE, STATICDIR, filename)
}

func UploadFile(uploadedFile multipart.File, fileext string, alias int) (*string, *string, error) {
	dir, err := os.Getwd()
	if err != nil {
		return nil, nil, errorHandler.GenerateError(err)
	}

	err = deleteFile(strconv.Itoa(alias))
	if err != nil {
		return nil, nil, errorHandler.GenerateError(err)
	}
	
	filename := fmt.Sprintf("%d%s", alias, fileext)
	fileLocationRelative := filepath.Join(STATICDIR, filename)
	fileLocation := filepath.Join(dir, STATICDIR, filename)

	targetFile, err := os.OpenFile(fileLocation, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		return nil, nil, errorHandler.GenerateError(err)
	}
	defer targetFile.Close()

	if _, err := io.Copy(targetFile, uploadedFile); err != nil {
		return nil, nil, errorHandler.GenerateError(err)
	}

	filenameURI := fileUri(filename)
	return &filenameURI, &fileLocationRelative, nil
}

func deleteFile(filePath string) error {
	files, err := filepath.Glob(fmt.Sprintf("%s/%s*", STATICDIR, filePath))
	if err != nil {
		return errorHandler.GenerateError(err)
	}

	for _,f := range files {
		if err := os.Remove(f); err != nil {
			return errorHandler.GenerateError(err)
		}
	}

	return nil
}