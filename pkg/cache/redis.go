package cache

import (
	"encoding/json"
	"log"
	"strings"
	"time"
	"github.com/go-redis/redis"
)

type Redis struct {
	client *redis.Client
}

func New(c *redis.Client) Redis {
	return Redis{c}
}

func (r Redis) Set(key string, val interface{}) {
	key = strings.Replace(key, " ", "_", -1)
	stringedVal, err := json.Marshal(val)
	if err != nil {
		log.Fatal(err)
	}

	err = r.client.Set(key, stringedVal, 10*time.Minute).Err()
	if err != nil {
		log.Fatal(err)
	}
}

func (r Redis) Get(key string) (interface {}){
	key = strings.Replace(key, " ", "_", -1)
	val, err := r.client.Get(key).Result()

	// key doesnt exist
	if err == redis.Nil {
		return nil
	}
	if err != nil {
		log.Fatal(err)
	}
	return val
}

func (r Redis) Del(key string) {
	key = strings.Replace(key, " ", "_", -1)
	err := r.client.Del(key).Err()
	if err != nil {
		log.Fatal(err)
	}
}

func (r Redis) Close() {
	r.client.Close()
}