package seed

import (
	"fmt"
	"database/sql"

	"gitlab.com/atthoriq/user-api/pkg/constants"
)

func Load(db *sql.DB) {
	numOfDataSeed := 5000000
	for i := 1; i <= numOfDataSeed; i++ {
		
		user := constants.User{
			Username: fmt.Sprintf("username %d", i),
			Password: "password",
			Nickname: fmt.Sprintf("name %d", i),
		}
		
		statement := fmt.Sprintf("INSERT INTO users(username, nickname, password) VALUES(%s,%s,%s)", user.Username, user.Nickname, user.Password)
		_, err := db.Exec(statement)
		if err != nil {
			panic(err)
		}

	}
	fmt.Printf("Load %d data done", numOfDataSeed)
}