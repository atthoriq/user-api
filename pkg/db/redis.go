package db

import (
	"fmt"

	"github.com/go-redis/redis"
	c "gitlab.com/atthoriq/user-api/configs"
)

var red *redis.Client

func init() {
	red = redis.NewClient(&redis.Options{
		Addr:     c.CacheServer(),
		Password: c.CachePassword(), 
		DB:       c.CacheDb(), 
	})

	pong, err := red.Ping().Result()
	fmt.Println(pong, err)
}

func GetCache() *redis.Client {
	return red
}