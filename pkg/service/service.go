package service

import (
	"fmt"
	"net/http"
	"mime/multipart"

	"gitlab.com/atthoriq/user-api/contracts"
	"gitlab.com/atthoriq/user-api/pkg/constants"
	"gitlab.com/atthoriq/user-api/pkg/helper/encrypt"
	"gitlab.com/atthoriq/user-api/pkg/helper/errorHandler"
	"gitlab.com/atthoriq/user-api/pkg/helper/jwt"
	"gitlab.com/atthoriq/user-api/pkg/helper/fileUploader"
)

type ServiceUseCase struct {
	Repository contracts.Repository
}

func New(repo contracts.Repository) *ServiceUseCase {
	return &ServiceUseCase{repo}
}

func generateUserOutput(user constants.User) constants.UserOutput {
	return constants.UserOutput{
		ID: user.ID,
		Username: user.Username,
		Nickname: user.Nickname,
		Profile_Pic: user.Profile_Pic,
	}
}

func (u *ServiceUseCase) Register(registerUser constants.RegisterUserPayload) (*constants.UserOutput, error) {
	encryptedPass, err := encrypt.EncodePassword(registerUser.Password)
	if err != nil {
		return nil, errorHandler.GenerateError(err)
	}

	savedUser := constants.User{
		Username: registerUser.Username,
		Nickname: registerUser.Nickname,
		Password: encryptedPass,
	}

	user, err := u.Repository.Register(savedUser)
	if err != nil {
		return nil, err
	} else {
		output := generateUserOutput(*user)
		return &output, nil
	}
}

func (u *ServiceUseCase) Login(loginPayload constants.LoginPayload) (*constants.LoginResponse, error) {
	user, err := u.Repository.GetUserByUsername(loginPayload.Username)
	if err != nil {
		return nil, errorHandler.GenerateError(err)
	}

	isPassword := encrypt.CheckPassword(user.Password, loginPayload.Password)
	if !isPassword {
		return nil, errorHandler.GenerateNewError(http.StatusBadRequest, "Username or Password Incorrect")
	}

	token, err := jwt.GenerateToken(jwt.TokenPayload{ID: user.ID, Username: user.Username})
	if err != nil {
		return nil, err
	}

	output := constants.LoginResponse{
		Token: *token,
		User: generateUserOutput(*user),
	}

	return &output, nil
}

func (u *ServiceUseCase) UpdateNickname(payload constants.UpdateNicknamePayload, userid int, username string) (*string, error) {
	row, err := u.Repository.UpdateNickname(payload.NewName, userid, username)
	if err != nil {
		return nil, err
	} else {
		output := fmt.Sprintf("Update successful to %d row(s).", *row)
		return &output, nil
	}
}

func (u *ServiceUseCase) UpdateProfilepic(uploadedFile multipart.File, fileext string, userid int) (*string, error) {
	fileLocation, fileLocRelative, err := fileUploader.UploadFile(uploadedFile, fileext, userid)
	if err != nil {
		return nil, errorHandler.GenerateError(err)
	}

	_, err = u.Repository.UpdateProfilepic(*fileLocRelative, userid)
	if err != nil {
		return nil, err
	}

	output := fmt.Sprintf("Update successful, picture path: %s", *fileLocation)
	return &output, nil
}

func (u *ServiceUseCase) GetProfile(username string) (*constants.UserOutput, error) {
	user, err := u.Repository.GetUserByUsername(username)
	if err != nil {
		return nil, errorHandler.GenerateError(err)
	}

	userOutput := generateUserOutput(*user)

	return &userOutput, nil
}