package service_test

import (
	"errors"
	"fmt"
	"testing"
	"image"
	"image/png"
	"os"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"

	"gitlab.com/atthoriq/user-api/contracts/mocks"
	"gitlab.com/atthoriq/user-api/pkg/constants"
	"gitlab.com/atthoriq/user-api/pkg/service"
	"gitlab.com/atthoriq/user-api/pkg/helper/encrypt"
)

const (
	username = "uname"
	password = "passwd"
	nickname = "nickname"

	username2 = "uname2"
	password2 = "passwd2"
	nickname2 = "nickname2"
)

var hashpass, _ = encrypt.EncodePassword(password)
var hashpass2, _ = encrypt.EncodePassword(password2)

var user = constants.User{
	ID: 1,
	Username: username,
	Password: hashpass,
	Nickname: nickname,
}

func TestRegister(t *testing.T) {
	mockRepository := new(mocks.Repository)

	s := service.New(mockRepository)
	registerPayload := constants.RegisterUserPayload{
		Username: username,
		Password: password,
		Nickname: nickname,
	}

	mockRepository.On("Register", mock.AnythingOfType("User")).Return(&user,nil)

	res, err := s.Register(registerPayload)

	assert.NoError(t, err)
	assert.NotNil(t, res)
	mockRepository.AssertExpectations(t)
}

func TestSuccessLogin(t *testing.T) {
	mockRepository := new(mocks.Repository)

	s := service.New(mockRepository)

	loginPayload := constants.LoginPayload{
		Username: username,
		Password: password,
	}

	mockRepository.On("GetUserByUsername", mock.AnythingOfType("string")).Return(&user,nil)

	res, err := s.Login(loginPayload)
	assert.NoError(t, err)
	assert.NotNil(t, res)
	mockRepository.AssertExpectations(t)
}

func TestLoginInvalidPassword(t *testing.T) {
	mockRepository := new(mocks.Repository)

	s := service.New(mockRepository)

	loginPayload := constants.LoginPayload{
		Username: username,
		Password: password2,
	}

	mockRepository.On("GetUserByUsername", mock.AnythingOfType("string")).Return(&user,nil)

	res, err := s.Login(loginPayload)
	assert.Error(t, err)
	assert.Nil(t, res)
	mockRepository.AssertExpectations(t)
}

func TestSuccessUpdateNickname(t *testing.T) {
	mockRepository := new(mocks.Repository)

	s := service.New(mockRepository)

	nicknamePayload := constants.UpdateNicknamePayload{
		NewName: nickname2,
	}

	expectedResult := int64(1)

	mockRepository.On("UpdateNickname", mock.AnythingOfType("string"), mock.AnythingOfType("int"), mock.AnythingOfType("string")).Return(&expectedResult,nil)

	res, err := s.UpdateNickname(nicknamePayload, int(user.ID), user.Username)
	fmt.Println(*res)
	assert.NoError(t, err)
	assert.NotNil(t, res)
	mockRepository.AssertExpectations(t)
}

func getFile(path string) (*os.File) {
	// Create a blank image 100x200 pixels
    myImage := image.NewRGBA(image.Rect(0, 0, 100, 200))

    // outputFile is a File type which satisfies Writer interface
    outputFile, err := os.Create(path)
    if err != nil {
    	panic(err)
    }

    // Encode takes a writer interface and an image interface
    // We pass it the File and the RGBA
	png.Encode(outputFile, myImage)
	
	return outputFile
}

func initTestUploadImage() (*os.File, string) {
	dir, _ := os.Getwd()
	path := fmt.Sprintf("%s/assets", dir)
	if _, err := os.Stat(path); os.IsNotExist(err) {
		os.Mkdir(path, 0777)
	}
	
	ext := ".png"
	filename := fmt.Sprintf("_%s", ext)
	filepath := fmt.Sprintf("%s/%s", path, filename)
	file := getFile(filepath)
	
	return file, ext
}

func closeTestUploadImage(file *os.File) {
	file.Close()
	dir, _ := os.Getwd()
	os.RemoveAll(fmt.Sprintf("%s/assets", dir))
}

func TestUploadImage(t *testing.T) {
	file, ext := initTestUploadImage()
	defer closeTestUploadImage(file)

	mockRepository := new(mocks.Repository)
	s := service.New(mockRepository)

	expectedResult := int64(1)
	mockRepository.On("UpdateProfilepic", mock.AnythingOfType("string"), mock.AnythingOfType("int")).Return(&expectedResult,nil)

	res, err := s.UpdateProfilepic(file, ext, -1)

	assert.NoError(t, err)
	assert.NotNil(t, res)
	mockRepository.AssertExpectations(t)
}

func TestGetProfile(t *testing.T) {
	mockRepository := new(mocks.Repository)
	s := service.New(mockRepository)

	mockRepository.On("GetUserByUsername", mock.AnythingOfType("string")).Return(&user,nil)

	res, err := s.GetProfile(user.Username)
	assert.NoError(t, err)
	assert.NotNil(t, res)
	mockRepository.AssertExpectations(t)
}

func TestGetProfileNoProfileMatch(t *testing.T) {
	mockRepository := new(mocks.Repository)
	s := service.New(mockRepository)

	mockRepository.On("GetUserByUsername", mock.AnythingOfType("string")).Return(nil,errors.New("some error"))

	res, err := s.GetProfile(user.Username)
	assert.Error(t, err)
	assert.Nil(t, res)
	mockRepository.AssertExpectations(t)
}