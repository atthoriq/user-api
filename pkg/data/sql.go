package data

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"
	"database/sql"

	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/atthoriq/user-api/pkg/constants"
	c "gitlab.com/atthoriq/user-api/pkg/cache"
	"gitlab.com/atthoriq/user-api/pkg/helper/errorHandler"
)

type UserRepository struct {
	db *sql.DB
	cache c.Redis
}

func New(db *sql.DB, redis c.Redis) *UserRepository {
	return &UserRepository{db, redis}
}

func (u *UserRepository) Register(user constants.User) (*constants.User,error) {
	db := u.db

	statement := fmt.Sprintf(constants.QInsertUser,user.Username, user.Nickname, user.Password )
	fmt.Println(statement)
	result, err := db.Exec(statement)
	if err != nil {
		if strings.Contains(err.Error(), "Duplicate entry") {
			return nil, errorHandler.GenerateNewError(http.StatusBadRequest, "Username already taken")
		}
		return nil, errorHandler.GenerateError(err)
	}

	insertId, err := result.LastInsertId()
	if err != nil {
		return nil, errorHandler.GenerateError(err)
	}

	user.ID = uint64(insertId)

	return &user, nil
}

func (u *UserRepository) GetUserByUsername(username string) (*constants.User,error) {
	db := u.db
	var user constants.User

	val := u.cache.Get(strings.Replace(username, " ", "_", -1))
	if val != nil {
		v, _ := val.(string)
		json.Unmarshal([]byte(v), &user)
		return &user, nil
	}

	statement, err := db.Prepare(constants.QGetUserByUsername)
	if err != nil {
		log.Println(err)
		return nil, errorHandler.GenerateError(err)
	}

	rows:= statement.QueryRow(username)
	err = rows.Scan(&user.ID, &user.Username, &user.Nickname, &user.Password, &user.Profile_Pic)
	if err != nil {
		log.Println(err)
		return nil, errorHandler.GenerateError(err)
	}

	statement.Close()

	u.cache.Set(username, &user)
	return &user, nil
}

func (u *UserRepository) UpdateNickname(nickname string, userid int, username string) (*int64, error) {
	db := u.db

	statement := fmt.Sprintf(constants.QUpdateNickname, nickname, userid)

	result, err := db.Exec(statement)
	if err != nil {
		return nil, errorHandler.GenerateError(err)
	}
	rows , err := result.RowsAffected()
	if err != nil {
		return nil, errorHandler.GenerateError(err)
	}

	u.cache.Del(username)
	return &rows, nil
}

func (u *UserRepository) UpdateProfilepic(path string, userid int) (*int64, error) {
	db := u.db

	statement := fmt.Sprintf(constants.QUpdateProfilepic, path, userid)

	result, err := db.Exec(statement)
	if err != nil {
		return nil, errorHandler.GenerateError(err)
	}
	rows , err := result.RowsAffected()
	if err != nil {
		return nil, errorHandler.GenerateError(err)
	}

	return &rows, nil
}