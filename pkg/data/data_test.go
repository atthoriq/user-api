package data_test

import (
	"fmt"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/alicebob/miniredis"
	"github.com/go-redis/redis"
	c "gitlab.com/atthoriq/user-api/pkg/cache"
	"gitlab.com/atthoriq/user-api/pkg/constants"
	"gitlab.com/atthoriq/user-api/pkg/data"
)

const (
	uname = "uname"
	password = "passwd"
	nickname = "nickname"
)

var user1 = constants.User{
	Username: uname,
	Password: password,
	Nickname: nickname,
}
var user2 = constants.User{
	Username: uname,
	Password: password,
	Nickname: nickname,
}

func TestSuccessRegister(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	s, err := miniredis.Run()
	if err != nil {
		panic(err)
	}
	defer s.Close()

	red := redis.NewClient(&redis.Options{
		Addr:     s.Addr(),
	})
	cache := c.New(red)

	mock.ExpectExec("^INSERT (.+)").WillReturnResult(sqlmock.NewResult(1, 1))
	
	repo := data.New(db, cache)
	_, err = repo.Register(user1)
	if err != nil {
		t.Errorf("error was not expected while registering user: %s", err)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}
}

func TestDuplicateUsernameRegister(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	s, err := miniredis.Run()
	if err != nil {
		panic(err)
	}
	defer s.Close()

	red := redis.NewClient(&redis.Options{
		Addr:     s.Addr(),
	})
	cache := c.New(red)

	repo := data.New(db, cache)

	mock.ExpectExec("^INSERT (.+)").WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectExec("^INSERT (.+)").WillReturnError(fmt.Errorf("some error"))

	_, err = repo.Register(user1)
	if err != nil {
		t.Errorf("error was not expected while registering user: %s", err)
	}

	_, err = repo.Register(user2)
	if err == nil {
		t.Errorf("error was not expected while registering user: %s", err)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}
}

func TestSuccessGetUserByUsername(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	s, err := miniredis.Run()
	if err != nil {
		panic(err)
	}
	defer s.Close()

	red := redis.NewClient(&redis.Options{
		Addr:     s.Addr(),
	})
	cache := c.New(red)

	repo := data.New(db, cache)

	_,_ = repo.Register(user1)

	rows := sqlmock.NewRows([]string{"userid", "username", "nickname", "password", "profile_pic"}).
			AddRow(1, user1.Username, user1.Nickname, user1.Password, "NULL")

	mock.ExpectPrepare("SELECT").ExpectQuery().WithArgs(user1.Username).WillReturnRows(rows)

	_,err = repo.GetUserByUsername(user1.Username)
	if err != nil {
		t.Errorf("error was not expected while getting user: %s", err)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}
}

func TestUsernameDoesntExist(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	s, err := miniredis.Run()
	if err != nil {
		panic(err)
	}
	defer s.Close()

	red := redis.NewClient(&redis.Options{
		Addr:     s.Addr(),
	})
	cache := c.New(red)

	repo := data.New(db, cache)

	_,_ = repo.Register(user1)

	mock.ExpectPrepare("SELECT").ExpectQuery().WithArgs(user2.Username).WillReturnError(fmt.Errorf("error getting user"))

	_,err = repo.GetUserByUsername(user1.Username)
	if err == nil {
		t.Errorf("error was not expected while getting user: %s", err)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}
}

func TestSuccessUpdateNickname(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	s, err := miniredis.Run()
	if err != nil {
		panic(err)
	}
	defer s.Close()

	red := redis.NewClient(&redis.Options{
		Addr:     s.Addr(),
	})
	cache := c.New(red)

	repo := data.New(db, cache)

	_,_ = repo.Register(user1)
	newNickname := "newNickname"

	// run update nickname function okay
	mock.ExpectExec("^UPDATE (.+)").WillReturnResult(sqlmock.NewResult(1, 1))
	_,err = repo.UpdateNickname(newNickname, 1, user1.Username)
	if err != nil {
		t.Errorf("error was not expected while update user nickname: %s", err)
	}

	// check the name changed
	rows := sqlmock.NewRows([]string{"userid", "username", "nickname", "password", "profile_pic"}).
			AddRow(1, user1.Username, newNickname, user1.Password, "NULL")

	mock.ExpectPrepare("SELECT").ExpectQuery().WithArgs(user1.Username).WillReturnRows(rows)
	_,err = repo.GetUserByUsername(user1.Username)
	if err != nil {
		t.Errorf("error was not expected while getting user: %s", err)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}
}

func TestSuccessUpdateProfilepic(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	s, err := miniredis.Run()
	if err != nil {
		panic(err)
	}
	defer s.Close()

	red := redis.NewClient(&redis.Options{
		Addr:     s.Addr(),
	})
	cache := c.New(red)

	repo := data.New(db, cache)

	_,_ = repo.Register(user1)
	newPic := "assets/1.png"

	// run update pic function okay
	mock.ExpectExec("^UPDATE (.+)").WillReturnResult(sqlmock.NewResult(1, 1))
	_,err = repo.UpdateProfilepic(newPic, 1)
	if err != nil {
		t.Errorf("error was not expected while update user nickname: %s", err)
	}

	// check the pic changed
	rows := sqlmock.NewRows([]string{"userid", "username", "nickname", "password", "profile_pic"}).
			AddRow(1, user1.Username, user1.Nickname, user1.Password, newPic)

	mock.ExpectPrepare("SELECT").ExpectQuery().WithArgs(user1.Username).WillReturnRows(rows)
	_,err = repo.GetUserByUsername(user1.Username)
	if err != nil {
		t.Errorf("error was not expected while getting user: %s", err)
	}
	
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}
}